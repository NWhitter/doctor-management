package ui;

import data.Practice;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Natasha Whitter on 02/05/2014.
 */
public class PracticeGui extends JFrame
{
    private static JLabel lPractice = new JLabel();
    private static JLabel lTelephone = new JLabel();

    public PracticeGui()
    {
        setFrameLayout();
    }

    public void setFrameLayout()
    {
        setLayout(null);
        setTitle("Doctor Management System");
        setLocation(100, 10);
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        lPractice.setBounds(50, 50, 250, 25);
        add(lPractice);

        lTelephone.setBounds(50, 100, 250, 25);
        add(lTelephone);

        JButton btnAdd = new JButton("Add Doctor");
        btnAdd.setBounds(50, 500-100, 150, 25);
        add(btnAdd);

        JButton btnRemove = new JButton("Remove Doctor");
        btnRemove.setBounds(500-225, 500-100, 150, 25);
        add(btnRemove);
    }

    public static void main(String[] args)
    {
        new PracticeGui().setVisible(true);
        Practice practice = newPractice();
        lPractice.setText("Practice Name : " + practice.getName());
        lTelephone.setText("Practice Number : " + practice.getTelephone());
    }

    public static Practice newPractice()
    {
        Practice practice = new Practice();
        practice.setName("Brighton Local Practice");
        practice.setTelephone("01584922361");
        return practice;
    }
}